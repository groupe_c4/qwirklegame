﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MenuQwirkle
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MenuQwirkle))
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmd_jouer = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmd_credit = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.cmd_quitter = New System.Windows.Forms.Button()
        Me.Pctx_regle = New System.Windows.Forms.PictureBox()
        Me.Pctx_quirkle = New System.Windows.Forms.PictureBox()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.Pctx_regle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pctx_quirkle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.cmd_jouer)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel2)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmd_credit)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel3)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmd_quitter)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(213, 163)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(2)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(233, 253)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'cmd_jouer
        '
        Me.cmd_jouer.BackColor = System.Drawing.Color.DodgerBlue
        Me.cmd_jouer.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmd_jouer.Font = New System.Drawing.Font("Yu Gothic UI", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_jouer.Location = New System.Drawing.Point(2, 2)
        Me.cmd_jouer.Margin = New System.Windows.Forms.Padding(2)
        Me.cmd_jouer.Name = "cmd_jouer"
        Me.cmd_jouer.Size = New System.Drawing.Size(230, 58)
        Me.cmd_jouer.TabIndex = 2
        Me.cmd_jouer.Text = "JOUER"
        Me.cmd_jouer.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(2, 64)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(230, 29)
        Me.Panel2.TabIndex = 3
        '
        'cmd_credit
        '
        Me.cmd_credit.BackColor = System.Drawing.Color.Yellow
        Me.cmd_credit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmd_credit.Font = New System.Drawing.Font("Yu Gothic UI", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_credit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmd_credit.Location = New System.Drawing.Point(2, 97)
        Me.cmd_credit.Margin = New System.Windows.Forms.Padding(2)
        Me.cmd_credit.Name = "cmd_credit"
        Me.cmd_credit.Size = New System.Drawing.Size(230, 58)
        Me.cmd_credit.TabIndex = 4
        Me.cmd_credit.Text = "CREDIT"
        Me.cmd_credit.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.Location = New System.Drawing.Point(2, 159)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(230, 29)
        Me.Panel3.TabIndex = 5
        '
        'cmd_quitter
        '
        Me.cmd_quitter.BackColor = System.Drawing.Color.Red
        Me.cmd_quitter.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmd_quitter.Font = New System.Drawing.Font("Yu Gothic UI", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_quitter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmd_quitter.Location = New System.Drawing.Point(2, 192)
        Me.cmd_quitter.Margin = New System.Windows.Forms.Padding(2)
        Me.cmd_quitter.Name = "cmd_quitter"
        Me.cmd_quitter.Size = New System.Drawing.Size(230, 58)
        Me.cmd_quitter.TabIndex = 6
        Me.cmd_quitter.Text = "QUITTER"
        Me.cmd_quitter.UseVisualStyleBackColor = False
        '
        'Pctx_regle
        '
        Me.Pctx_regle.BackColor = System.Drawing.Color.Transparent
        Me.Pctx_regle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Pctx_regle.Image = Global.QwirkleInterface.My.Resources.Resources.Itrg
        Me.Pctx_regle.Location = New System.Drawing.Point(585, 466)
        Me.Pctx_regle.Name = "Pctx_regle"
        Me.Pctx_regle.Size = New System.Drawing.Size(61, 58)
        Me.Pctx_regle.TabIndex = 2
        Me.Pctx_regle.TabStop = False
        '
        'Pctx_quirkle
        '
        Me.Pctx_quirkle.BackColor = System.Drawing.Color.Transparent
        Me.Pctx_quirkle.Image = Global.QwirkleInterface.My.Resources.Resources.Logo
        Me.Pctx_quirkle.Location = New System.Drawing.Point(230, -2)
        Me.Pctx_quirkle.Name = "Pctx_quirkle"
        Me.Pctx_quirkle.Size = New System.Drawing.Size(199, 162)
        Me.Pctx_quirkle.TabIndex = 3
        Me.Pctx_quirkle.TabStop = False
        '
        'MenuQwirkle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gray
        Me.BackgroundImage = Global.QwirkleInterface.My.Resources.Resources.wallpaper
        Me.ClientSize = New System.Drawing.Size(658, 536)
        Me.Controls.Add(Me.Pctx_quirkle)
        Me.Controls.Add(Me.Pctx_regle)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "MenuQwirkle"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Qwirkle"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.Pctx_regle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pctx_quirkle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents cmd_jouer As Button
    Friend WithEvents Panel2 As Panel
    Friend WithEvents cmd_credit As Button
    Friend WithEvents Panel3 As Panel
    Friend WithEvents cmd_quitter As Button
    Friend WithEvents Pctx_regle As PictureBox
    Friend WithEvents Pctx_quirkle As PictureBox
End Class
