﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OrdreDesJoueurs
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OrdreDesJoueurs))
        Me.lbl_prenom = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cbx_Nbpions = New System.Windows.Forms.ComboBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmd_valider = New System.Windows.Forms.Button()
        Me.img_pion1 = New System.Windows.Forms.PictureBox()
        Me.img_pion2 = New System.Windows.Forms.PictureBox()
        Me.img_pion3 = New System.Windows.Forms.PictureBox()
        Me.img_pion4 = New System.Windows.Forms.PictureBox()
        Me.img_pion5 = New System.Windows.Forms.PictureBox()
        Me.img_pion6 = New System.Windows.Forms.PictureBox()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.img_pion1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_pion2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_pion3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_pion4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_pion5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_pion6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_prenom
        '
        Me.lbl_prenom.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_prenom.AutoSize = True
        Me.lbl_prenom.BackColor = System.Drawing.Color.Transparent
        Me.lbl_prenom.Font = New System.Drawing.Font("Yu Gothic UI", 33.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_prenom.ForeColor = System.Drawing.Color.White
        Me.lbl_prenom.Location = New System.Drawing.Point(220, 69)
        Me.lbl_prenom.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_prenom.Name = "lbl_prenom"
        Me.lbl_prenom.Size = New System.Drawing.Size(219, 120)
        Me.lbl_prenom.TabIndex = 3
        Me.lbl_prenom.Text = "C’est à  " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "de choisir"
        Me.lbl_prenom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Yu Gothic UI", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(98, 278)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(459, 45)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nombre de pions max jouable"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.cbx_Nbpions)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel2)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmd_valider)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(212, 346)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(2)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(234, 154)
        Me.FlowLayoutPanel1.TabIndex = 5
        '
        'cbx_Nbpions
        '
        Me.cbx_Nbpions.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cbx_Nbpions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Nbpions.Font = New System.Drawing.Font("Yu Gothic UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbx_Nbpions.ForeColor = System.Drawing.Color.Black
        Me.cbx_Nbpions.FormattingEnabled = True
        Me.cbx_Nbpions.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6"})
        Me.cbx_Nbpions.Location = New System.Drawing.Point(2, 2)
        Me.cbx_Nbpions.Margin = New System.Windows.Forms.Padding(2)
        Me.cbx_Nbpions.Name = "cbx_Nbpions"
        Me.cbx_Nbpions.Size = New System.Drawing.Size(232, 45)
        Me.cbx_Nbpions.Sorted = True
        Me.cbx_Nbpions.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(2, 51)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(230, 42)
        Me.Panel2.TabIndex = 6
        '
        'cmd_valider
        '
        Me.cmd_valider.BackColor = System.Drawing.Color.DodgerBlue
        Me.cmd_valider.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmd_valider.Font = New System.Drawing.Font("Yu Gothic UI", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_valider.Location = New System.Drawing.Point(2, 97)
        Me.cmd_valider.Margin = New System.Windows.Forms.Padding(2)
        Me.cmd_valider.Name = "cmd_valider"
        Me.cmd_valider.Size = New System.Drawing.Size(230, 58)
        Me.cmd_valider.TabIndex = 5
        Me.cmd_valider.Text = "VALIDER"
        Me.cmd_valider.UseVisualStyleBackColor = False
        '
        'img_pion1
        '
        Me.img_pion1.Location = New System.Drawing.Point(2, 2)
        Me.img_pion1.Margin = New System.Windows.Forms.Padding(2)
        Me.img_pion1.Name = "img_pion1"
        Me.img_pion1.Size = New System.Drawing.Size(40, 40)
        Me.img_pion1.TabIndex = 6
        Me.img_pion1.TabStop = False
        '
        'img_pion2
        '
        Me.img_pion2.Location = New System.Drawing.Point(46, 2)
        Me.img_pion2.Margin = New System.Windows.Forms.Padding(2)
        Me.img_pion2.Name = "img_pion2"
        Me.img_pion2.Size = New System.Drawing.Size(40, 40)
        Me.img_pion2.TabIndex = 7
        Me.img_pion2.TabStop = False
        '
        'img_pion3
        '
        Me.img_pion3.Location = New System.Drawing.Point(90, 2)
        Me.img_pion3.Margin = New System.Windows.Forms.Padding(2)
        Me.img_pion3.Name = "img_pion3"
        Me.img_pion3.Size = New System.Drawing.Size(40, 40)
        Me.img_pion3.TabIndex = 8
        Me.img_pion3.TabStop = False
        '
        'img_pion4
        '
        Me.img_pion4.Location = New System.Drawing.Point(134, 2)
        Me.img_pion4.Margin = New System.Windows.Forms.Padding(2)
        Me.img_pion4.Name = "img_pion4"
        Me.img_pion4.Size = New System.Drawing.Size(40, 40)
        Me.img_pion4.TabIndex = 9
        Me.img_pion4.TabStop = False
        '
        'img_pion5
        '
        Me.img_pion5.Location = New System.Drawing.Point(178, 2)
        Me.img_pion5.Margin = New System.Windows.Forms.Padding(2)
        Me.img_pion5.Name = "img_pion5"
        Me.img_pion5.Size = New System.Drawing.Size(40, 40)
        Me.img_pion5.TabIndex = 10
        Me.img_pion5.TabStop = False
        '
        'img_pion6
        '
        Me.img_pion6.Location = New System.Drawing.Point(222, 2)
        Me.img_pion6.Margin = New System.Windows.Forms.Padding(2)
        Me.img_pion6.Name = "img_pion6"
        Me.img_pion6.Size = New System.Drawing.Size(40, 40)
        Me.img_pion6.TabIndex = 11
        Me.img_pion6.TabStop = False
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.BackColor = System.Drawing.SystemColors.Window
        Me.FlowLayoutPanel2.Controls.Add(Me.img_pion1)
        Me.FlowLayoutPanel2.Controls.Add(Me.img_pion2)
        Me.FlowLayoutPanel2.Controls.Add(Me.img_pion3)
        Me.FlowLayoutPanel2.Controls.Add(Me.img_pion4)
        Me.FlowLayoutPanel2.Controls.Add(Me.img_pion5)
        Me.FlowLayoutPanel2.Controls.Add(Me.img_pion6)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(210, 203)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(2)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(265, 45)
        Me.FlowLayoutPanel2.TabIndex = 12
        '
        'OrdreDesJoueurs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.QwirkleInterface.My.Resources.Resources.wallpaper
        Me.ClientSize = New System.Drawing.Size(658, 536)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lbl_prenom)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "OrdreDesJoueurs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ordre Des Joueurs"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.img_pion1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_pion2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_pion3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_pion4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_pion5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_pion6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_prenom As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents cbx_Nbpions As ComboBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents cmd_valider As Button
    Friend WithEvents img_pion1 As PictureBox
    Friend WithEvents img_pion2 As PictureBox
    Friend WithEvents img_pion3 As PictureBox
    Friend WithEvents img_pion4 As PictureBox
    Friend WithEvents img_pion5 As PictureBox
    Friend WithEvents img_pion6 As PictureBox
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
End Class
