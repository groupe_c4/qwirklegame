﻿Imports QwirkleLibrary
Public Class OrdreDesJoueurs
    Dim PicDeckJoueur(5) As PictureBox
    Dim compteurJoueur As Integer = 0
    Public ListePionsPosables As List(Of Integer) = New List(Of Integer)
    Private Sub OrdreDesJoueurs_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbx_Nbpions.SelectedIndex = 0
        PicDeckJoueur(0) = img_pion1
        PicDeckJoueur(1) = img_pion2
        PicDeckJoueur(2) = img_pion3
        PicDeckJoueur(3) = img_pion4
        PicDeckJoueur(4) = img_pion5
        PicDeckJoueur(5) = img_pion6
        lbl_prenom.Text = "C'est à " + SaisieAgePrenom.Joueurs(0).getName & vbCrLf + "de choisir !"
        For nbrePion As Integer = 0 To 5
            PicDeckJoueur(nbrePion).Image = My.Resources.ResourceManager.GetObject(SaisieAgePrenom.Joueurs(0).getPionI(nbrePion))
        Next
    End Sub

    Private Sub Cmd_valider_Click(sender As Object, e As EventArgs) Handles cmd_valider.Click
        ListePionsPosables.Add(Integer.Parse(cbx_Nbpions.Text))
        compteurJoueur += 1
        If (compteurJoueur = Integer.Parse(SelectionNombreJoueur.cbx_Nbjoueur.Text)) Then
            Plateau.Show()
            Me.Hide()
        Else
            lbl_prenom.Text = "C'est à " + SaisieAgePrenom.Joueurs(compteurJoueur).getName & vbCrLf + "de choisir !"
            For nbrePion As Integer = 0 To 5
                PicDeckJoueur(nbrePion).Image = My.Resources.ResourceManager.GetObject(SaisieAgePrenom.Joueurs(compteurJoueur).getPionI(nbrePion))
            Next
        End If
    End Sub
End Class