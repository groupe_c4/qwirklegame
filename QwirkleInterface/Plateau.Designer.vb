﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Plateau
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Plateau))
        Me.lbl_classJ1 = New System.Windows.Forms.Label()
        Me.lbl_J1Point = New System.Windows.Forms.Label()
        Me.lbl_J2Point = New System.Windows.Forms.Label()
        Me.lbl_classJ2 = New System.Windows.Forms.Label()
        Me.lbl_J3Point = New System.Windows.Forms.Label()
        Me.lbl_classJ3 = New System.Windows.Forms.Label()
        Me.lbl_J4Point = New System.Windows.Forms.Label()
        Me.lbl_classJ4 = New System.Windows.Forms.Label()
        Me.pnl_grilleJeu = New System.Windows.Forms.Panel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.pic_pions6 = New System.Windows.Forms.PictureBox()
        Me.pic_pions5 = New System.Windows.Forms.PictureBox()
        Me.pic_pions4 = New System.Windows.Forms.PictureBox()
        Me.pic_pions3 = New System.Windows.Forms.PictureBox()
        Me.pic_pions2 = New System.Windows.Forms.PictureBox()
        Me.pic_pions1 = New System.Windows.Forms.PictureBox()
        Me.Btn_annuler = New System.Windows.Forms.Button()
        Me.pic_corbeille = New System.Windows.Forms.PictureBox()
        Me.btn_valider = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lbl_deckJ = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmd_menu = New System.Windows.Forms.Button()
        Me.Pctx_regle = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lbl_compteurpioche = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.pic_pions6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_pions5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_pions4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_pions3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_pions2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_pions1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_corbeille, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.Pctx_regle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_classJ1
        '
        Me.lbl_classJ1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_classJ1.AutoSize = True
        Me.lbl_classJ1.BackColor = System.Drawing.Color.Transparent
        Me.lbl_classJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_classJ1.ForeColor = System.Drawing.Color.Black
        Me.lbl_classJ1.Location = New System.Drawing.Point(4, 139)
        Me.lbl_classJ1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_classJ1.Name = "lbl_classJ1"
        Me.lbl_classJ1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_classJ1.Size = New System.Drawing.Size(82, 20)
        Me.lbl_classJ1.TabIndex = 9
        Me.lbl_classJ1.Text = "Joueur 1"
        '
        'lbl_J1Point
        '
        Me.lbl_J1Point.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_J1Point.AutoSize = True
        Me.lbl_J1Point.BackColor = System.Drawing.Color.Transparent
        Me.lbl_J1Point.ForeColor = System.Drawing.Color.Black
        Me.lbl_J1Point.Location = New System.Drawing.Point(41, 170)
        Me.lbl_J1Point.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_J1Point.Name = "lbl_J1Point"
        Me.lbl_J1Point.Size = New System.Drawing.Size(16, 17)
        Me.lbl_J1Point.TabIndex = 10
        Me.lbl_J1Point.Text = "0"
        '
        'lbl_J2Point
        '
        Me.lbl_J2Point.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_J2Point.AutoSize = True
        Me.lbl_J2Point.BackColor = System.Drawing.Color.Transparent
        Me.lbl_J2Point.ForeColor = System.Drawing.Color.Black
        Me.lbl_J2Point.Location = New System.Drawing.Point(41, 228)
        Me.lbl_J2Point.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_J2Point.Name = "lbl_J2Point"
        Me.lbl_J2Point.Size = New System.Drawing.Size(16, 17)
        Me.lbl_J2Point.TabIndex = 12
        Me.lbl_J2Point.Text = "0"
        '
        'lbl_classJ2
        '
        Me.lbl_classJ2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_classJ2.AutoSize = True
        Me.lbl_classJ2.BackColor = System.Drawing.Color.Transparent
        Me.lbl_classJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_classJ2.ForeColor = System.Drawing.Color.Black
        Me.lbl_classJ2.Location = New System.Drawing.Point(4, 197)
        Me.lbl_classJ2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_classJ2.Name = "lbl_classJ2"
        Me.lbl_classJ2.Size = New System.Drawing.Size(82, 20)
        Me.lbl_classJ2.TabIndex = 11
        Me.lbl_classJ2.Text = "Joueur 2"
        '
        'lbl_J3Point
        '
        Me.lbl_J3Point.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_J3Point.AutoSize = True
        Me.lbl_J3Point.BackColor = System.Drawing.Color.Transparent
        Me.lbl_J3Point.ForeColor = System.Drawing.Color.Black
        Me.lbl_J3Point.Location = New System.Drawing.Point(41, 286)
        Me.lbl_J3Point.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_J3Point.Name = "lbl_J3Point"
        Me.lbl_J3Point.Size = New System.Drawing.Size(16, 17)
        Me.lbl_J3Point.TabIndex = 14
        Me.lbl_J3Point.Text = "0"
        '
        'lbl_classJ3
        '
        Me.lbl_classJ3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_classJ3.AutoSize = True
        Me.lbl_classJ3.BackColor = System.Drawing.Color.Transparent
        Me.lbl_classJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_classJ3.ForeColor = System.Drawing.Color.Black
        Me.lbl_classJ3.Location = New System.Drawing.Point(4, 255)
        Me.lbl_classJ3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_classJ3.Name = "lbl_classJ3"
        Me.lbl_classJ3.Size = New System.Drawing.Size(82, 20)
        Me.lbl_classJ3.TabIndex = 13
        Me.lbl_classJ3.Text = "Joueur 3"
        '
        'lbl_J4Point
        '
        Me.lbl_J4Point.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_J4Point.AutoSize = True
        Me.lbl_J4Point.BackColor = System.Drawing.Color.Transparent
        Me.lbl_J4Point.ForeColor = System.Drawing.Color.Black
        Me.lbl_J4Point.Location = New System.Drawing.Point(41, 343)
        Me.lbl_J4Point.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_J4Point.Name = "lbl_J4Point"
        Me.lbl_J4Point.Size = New System.Drawing.Size(16, 17)
        Me.lbl_J4Point.TabIndex = 16
        Me.lbl_J4Point.Text = "0"
        '
        'lbl_classJ4
        '
        Me.lbl_classJ4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_classJ4.AutoSize = True
        Me.lbl_classJ4.BackColor = System.Drawing.Color.Transparent
        Me.lbl_classJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_classJ4.ForeColor = System.Drawing.Color.Black
        Me.lbl_classJ4.Location = New System.Drawing.Point(4, 313)
        Me.lbl_classJ4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_classJ4.Name = "lbl_classJ4"
        Me.lbl_classJ4.Size = New System.Drawing.Size(82, 20)
        Me.lbl_classJ4.TabIndex = 15
        Me.lbl_classJ4.Text = "Joueur 4"
        '
        'pnl_grilleJeu
        '
        Me.pnl_grilleJeu.AutoScroll = True
        Me.pnl_grilleJeu.Location = New System.Drawing.Point(16, 15)
        Me.pnl_grilleJeu.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pnl_grilleJeu.Name = "pnl_grilleJeu"
        Me.pnl_grilleJeu.Size = New System.Drawing.Size(533, 492)
        Me.pnl_grilleJeu.TabIndex = 7
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.pic_pions6)
        Me.FlowLayoutPanel1.Controls.Add(Me.pic_pions5)
        Me.FlowLayoutPanel1.Controls.Add(Me.pic_pions4)
        Me.FlowLayoutPanel1.Controls.Add(Me.pic_pions3)
        Me.FlowLayoutPanel1.Controls.Add(Me.pic_pions2)
        Me.FlowLayoutPanel1.Controls.Add(Me.pic_pions1)
        Me.FlowLayoutPanel1.ForeColor = System.Drawing.Color.Black
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(7, 49)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(320, 49)
        Me.FlowLayoutPanel1.TabIndex = 18
        '
        'pic_pions6
        '
        Me.pic_pions6.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pic_pions6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_pions6.Location = New System.Drawing.Point(0, 0)
        Me.pic_pions6.Margin = New System.Windows.Forms.Padding(0)
        Me.pic_pions6.MaximumSize = New System.Drawing.Size(53, 49)
        Me.pic_pions6.MinimumSize = New System.Drawing.Size(53, 49)
        Me.pic_pions6.Name = "pic_pions6"
        Me.pic_pions6.Size = New System.Drawing.Size(53, 49)
        Me.pic_pions6.TabIndex = 6
        Me.pic_pions6.TabStop = False
        '
        'pic_pions5
        '
        Me.pic_pions5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pic_pions5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_pions5.Location = New System.Drawing.Point(53, 0)
        Me.pic_pions5.Margin = New System.Windows.Forms.Padding(0)
        Me.pic_pions5.MaximumSize = New System.Drawing.Size(53, 49)
        Me.pic_pions5.MinimumSize = New System.Drawing.Size(53, 49)
        Me.pic_pions5.Name = "pic_pions5"
        Me.pic_pions5.Size = New System.Drawing.Size(53, 49)
        Me.pic_pions5.TabIndex = 5
        Me.pic_pions5.TabStop = False
        '
        'pic_pions4
        '
        Me.pic_pions4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pic_pions4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_pions4.Location = New System.Drawing.Point(106, 0)
        Me.pic_pions4.Margin = New System.Windows.Forms.Padding(0)
        Me.pic_pions4.MaximumSize = New System.Drawing.Size(53, 49)
        Me.pic_pions4.MinimumSize = New System.Drawing.Size(53, 49)
        Me.pic_pions4.Name = "pic_pions4"
        Me.pic_pions4.Size = New System.Drawing.Size(53, 49)
        Me.pic_pions4.TabIndex = 4
        Me.pic_pions4.TabStop = False
        '
        'pic_pions3
        '
        Me.pic_pions3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pic_pions3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_pions3.Location = New System.Drawing.Point(159, 0)
        Me.pic_pions3.Margin = New System.Windows.Forms.Padding(0)
        Me.pic_pions3.MaximumSize = New System.Drawing.Size(53, 49)
        Me.pic_pions3.MinimumSize = New System.Drawing.Size(53, 49)
        Me.pic_pions3.Name = "pic_pions3"
        Me.pic_pions3.Size = New System.Drawing.Size(53, 49)
        Me.pic_pions3.TabIndex = 3
        Me.pic_pions3.TabStop = False
        '
        'pic_pions2
        '
        Me.pic_pions2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pic_pions2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_pions2.Location = New System.Drawing.Point(212, 0)
        Me.pic_pions2.Margin = New System.Windows.Forms.Padding(0)
        Me.pic_pions2.MaximumSize = New System.Drawing.Size(53, 49)
        Me.pic_pions2.MinimumSize = New System.Drawing.Size(53, 49)
        Me.pic_pions2.Name = "pic_pions2"
        Me.pic_pions2.Size = New System.Drawing.Size(53, 49)
        Me.pic_pions2.TabIndex = 2
        Me.pic_pions2.TabStop = False
        '
        'pic_pions1
        '
        Me.pic_pions1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pic_pions1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_pions1.Location = New System.Drawing.Point(265, 0)
        Me.pic_pions1.Margin = New System.Windows.Forms.Padding(0)
        Me.pic_pions1.MaximumSize = New System.Drawing.Size(53, 49)
        Me.pic_pions1.MinimumSize = New System.Drawing.Size(53, 49)
        Me.pic_pions1.Name = "pic_pions1"
        Me.pic_pions1.Size = New System.Drawing.Size(53, 49)
        Me.pic_pions1.TabIndex = 1
        Me.pic_pions1.TabStop = False
        '
        'Btn_annuler
        '
        Me.Btn_annuler.BackColor = System.Drawing.Color.Transparent
        Me.Btn_annuler.BackgroundImage = Global.QwirkleInterface.My.Resources.Resources.btn_fleche2
        Me.Btn_annuler.Location = New System.Drawing.Point(396, 49)
        Me.Btn_annuler.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Btn_annuler.Name = "Btn_annuler"
        Me.Btn_annuler.Size = New System.Drawing.Size(53, 49)
        Me.Btn_annuler.TabIndex = 20
        Me.Btn_annuler.UseVisualStyleBackColor = False
        '
        'pic_corbeille
        '
        Me.pic_corbeille.BackColor = System.Drawing.Color.Red
        Me.pic_corbeille.BackgroundImage = Global.QwirkleInterface.My.Resources.Resources.btn_poubelle
        Me.pic_corbeille.Location = New System.Drawing.Point(335, 49)
        Me.pic_corbeille.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pic_corbeille.Name = "pic_corbeille"
        Me.pic_corbeille.Size = New System.Drawing.Size(53, 49)
        Me.pic_corbeille.TabIndex = 19
        Me.pic_corbeille.TabStop = False
        '
        'btn_valider
        '
        Me.btn_valider.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_valider.BackColor = System.Drawing.Color.Transparent
        Me.btn_valider.BackgroundImage = Global.QwirkleInterface.My.Resources.Resources.btn_valider1
        Me.btn_valider.ForeColor = System.Drawing.Color.Black
        Me.btn_valider.Location = New System.Drawing.Point(457, 49)
        Me.btn_valider.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btn_valider.Name = "btn_valider"
        Me.btn_valider.Size = New System.Drawing.Size(53, 49)
        Me.btn_valider.TabIndex = 17
        Me.btn_valider.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Silver
        Me.Panel1.Controls.Add(Me.FlowLayoutPanel1)
        Me.Panel1.Controls.Add(Me.Btn_annuler)
        Me.Panel1.Controls.Add(Me.btn_valider)
        Me.Panel1.Controls.Add(Me.lbl_deckJ)
        Me.Panel1.Controls.Add(Me.pic_corbeille)
        Me.Panel1.Location = New System.Drawing.Point(16, 514)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(533, 113)
        Me.Panel1.TabIndex = 21
        '
        'lbl_deckJ
        '
        Me.lbl_deckJ.AutoSize = True
        Me.lbl_deckJ.BackColor = System.Drawing.Color.Transparent
        Me.lbl_deckJ.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.lbl_deckJ.Font = New System.Drawing.Font("Yu Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_deckJ.ForeColor = System.Drawing.Color.Black
        Me.lbl_deckJ.Location = New System.Drawing.Point(4, 15)
        Me.lbl_deckJ.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_deckJ.Name = "lbl_deckJ"
        Me.lbl_deckJ.Size = New System.Drawing.Size(74, 31)
        Me.lbl_deckJ.TabIndex = 8
        Me.lbl_deckJ.Text = "Deck"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Orange
        Me.Panel2.Controls.Add(Me.cmd_menu)
        Me.Panel2.Controls.Add(Me.Pctx_regle)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Controls.Add(Me.lbl_classJ1)
        Me.Panel2.Controls.Add(Me.lbl_J4Point)
        Me.Panel2.Controls.Add(Me.lbl_J1Point)
        Me.Panel2.Controls.Add(Me.lbl_classJ4)
        Me.Panel2.Controls.Add(Me.lbl_classJ2)
        Me.Panel2.Controls.Add(Me.lbl_J3Point)
        Me.Panel2.Controls.Add(Me.lbl_J2Point)
        Me.Panel2.Controls.Add(Me.lbl_classJ3)
        Me.Panel2.ForeColor = System.Drawing.Color.Black
        Me.Panel2.Location = New System.Drawing.Point(557, 16)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(219, 612)
        Me.Panel2.TabIndex = 22
        '
        'cmd_menu
        '
        Me.cmd_menu.BackColor = System.Drawing.Color.Red
        Me.cmd_menu.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmd_menu.Font = New System.Drawing.Font("Yu Gothic UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_menu.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmd_menu.Location = New System.Drawing.Point(108, 538)
        Me.cmd_menu.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmd_menu.Name = "cmd_menu"
        Me.cmd_menu.Size = New System.Drawing.Size(107, 71)
        Me.cmd_menu.TabIndex = 22
        Me.cmd_menu.Text = "Menu"
        Me.cmd_menu.UseVisualStyleBackColor = False
        '
        'Pctx_regle
        '
        Me.Pctx_regle.BackColor = System.Drawing.Color.Transparent
        Me.Pctx_regle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Pctx_regle.Image = Global.QwirkleInterface.My.Resources.Resources.Itrg
        Me.Pctx_regle.Location = New System.Drawing.Point(20, 538)
        Me.Pctx_regle.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Pctx_regle.Name = "Pctx_regle"
        Me.Pctx_regle.Size = New System.Drawing.Size(81, 78)
        Me.Pctx_regle.TabIndex = 21
        Me.Pctx_regle.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackgroundImage = CType(resources.GetObject("Panel3.BackgroundImage"), System.Drawing.Image)
        Me.Panel3.Controls.Add(Me.lbl_compteurpioche)
        Me.Panel3.Location = New System.Drawing.Point(20, 363)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(173, 160)
        Me.Panel3.TabIndex = 21
        '
        'lbl_compteurpioche
        '
        Me.lbl_compteurpioche.AutoSize = True
        Me.lbl_compteurpioche.BackColor = System.Drawing.Color.Transparent
        Me.lbl_compteurpioche.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_compteurpioche.Location = New System.Drawing.Point(67, 71)
        Me.lbl_compteurpioche.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_compteurpioche.Name = "lbl_compteurpioche"
        Me.lbl_compteurpioche.Size = New System.Drawing.Size(37, 36)
        Me.lbl_compteurpioche.TabIndex = 20
        Me.lbl_compteurpioche.Text = "X"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(4, 5)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(211, 114)
        Me.PictureBox1.TabIndex = 18
        Me.PictureBox1.TabStop = False
        '
        'Plateau
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.Black
        Me.BackgroundImage = Global.QwirkleInterface.My.Resources.Resources.wood_wallpaper1
        Me.ClientSize = New System.Drawing.Size(791, 640)
        Me.Controls.Add(Me.pnl_grilleJeu)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "Plateau"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Plateau"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.pic_pions6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_pions5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_pions4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_pions3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_pions2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_pions1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_corbeille, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.Pctx_regle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pic_pions1 As PictureBox
    Friend WithEvents pic_pions2 As PictureBox
    Friend WithEvents pic_pions3 As PictureBox
    Friend WithEvents pic_pions4 As PictureBox
    Friend WithEvents pic_pions5 As PictureBox
    Friend WithEvents pic_pions6 As PictureBox
    Friend WithEvents lbl_classJ1 As Label
    Friend WithEvents lbl_J1Point As Label
    Friend WithEvents lbl_J2Point As Label
    Friend WithEvents lbl_classJ2 As Label
    Friend WithEvents lbl_J3Point As Label
    Friend WithEvents lbl_classJ3 As Label
    Friend WithEvents lbl_J4Point As Label
    Friend WithEvents lbl_classJ4 As Label
    Friend WithEvents btn_valider As Button
    Friend WithEvents pnl_grilleJeu As Panel
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents pic_corbeille As PictureBox
    Friend WithEvents Btn_annuler As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents lbl_deckJ As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents lbl_compteurpioche As Label
    Friend WithEvents Pctx_regle As PictureBox
    Friend WithEvents cmd_menu As Button
End Class
