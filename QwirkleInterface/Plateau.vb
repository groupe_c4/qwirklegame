﻿Imports QwirkleLibrary
Public Class Plateau
    Dim x As Byte
    Dim y As Byte
    Dim gridgame As Integer = 10
    Dim firstPoint As Boolean = False
    Public Picturebox As List(Of PictureBox) = New List(Of PictureBox)
    Public PositionPionX As List(Of Integer) = New List(Of Integer)
    Public PositionPionY As List(Of Integer) = New List(Of Integer)
    Public PictureBoxTemporary As List(Of PictureBox) = New List(Of PictureBox)
    Public picboxjoue As List(Of PictureBox) = New List(Of PictureBox)
    Dim PicDeckJoueur(5) As PictureBox
    Dim compteurJoueur As Integer = 1
    Dim numjoueur As Integer = 0
    Dim firstpart As Boolean = True
    Dim ListeOrdreJoueurs As List(Of Integer) = New List(Of Integer)
    Dim parcours As Integer = 0
    Dim memoire As Integer = 0
    Dim var As Integer
    Private Sub pic_pions_MouseMove(sender As Object, e As MouseEventArgs) Handles pic_pions1.MouseMove, pic_pions6.MouseMove, pic_pions5.MouseMove, pic_pions4.MouseMove, pic_pions3.MouseMove, pic_pions2.MouseMove
        Dim pic As PictureBox = sender
        Dim rep As DragDropEffects
        If e.Button = MouseButtons.Left AndAlso pic.Image IsNot Nothing Then
            pic.AllowDrop = False
            rep = pic.DoDragDrop(pic.Image, DragDropEffects.Move)
            If rep = DragDropEffects.Move Then
                pic.Image = Nothing
            End If
            pic.AllowDrop = True
        End If
    End Sub
    Private Sub picJeu_MouseMove(sender As Object, e As MouseEventArgs) 'MouseMove des pictures box de la grille de jeu
        Dim pic As PictureBox = sender
        Dim rep As DragDropEffects
        If e.Button = MouseButtons.Left AndAlso pic.Image IsNot Nothing Then
            pic.AllowDrop = False
            rep = pic.DoDragDrop(pic.Image, DragDropEffects.Move)
            If rep = DragDropEffects.Move Then
                pic.Image = Nothing
            End If
            pic.AllowDrop = True
        End If
    End Sub
    Private Sub picGrille_DragEnter(sender As Object, e As DragEventArgs)   'DragEnter des pictures box de la grille de jeu
        Dim pic As PictureBox = sender
        If pic.Image IsNot Nothing Then
            pic.AllowDrop = False
        Else
            If e.Data.GetDataPresent(DataFormats.Bitmap) Then
                e.Effect = DragDropEffects.Move
            Else
                e.Effect = DragDropEffects.None
            End If
        End If
    End Sub

    Private Sub picTableau_DragDrop(sender As Object, e As DragEventArgs)   'DragDrop des pictures box de la grille de jeu
        'Dim pic As PictureBox = sender
        'pic.Image = e.Data.GetData(DataFormats.Bitmap)
        'Dim var As Integer = 0

        'If firstPoint = True Then
        'For y = 0 To gridgame
        '    For x = 0 To gridgame
        '        var += 1
        '   Next
        'Next
        '    firstPoint = False
        'End If
        Dim pic As PictureBox = sender
        pic.Image = e.Data.GetData(DataFormats.Bitmap)
        PictureBoxTemporary.Add(pic)
        picboxjoue.Add(pic)
    End Sub

    Private Sub Plateau_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lbl_compteurpioche.Text = SaisieAgePrenom.deck.getNombreDeck()
        For x = 0 To gridgame
            For y = 0 To gridgame
                Dim newpic As New PictureBox With {
                    .BorderStyle = BorderStyle.FixedSingle,
                    .Name = "pic" & x & y,
                    .Size = New Size(40, 40)
                }
                Picturebox.Add(newpic)
                If x = 5 And y = 5 Then
                    newpic.Location = New Point((40 * (x)), (40 * (y)))
                    pnl_grilleJeu.Controls.Add(newpic)
                    newpic.AllowDrop = True
                    newpic.BackColor = Color.Green
                    firstPoint = True
                Else
                    newpic.Location = New Point((40 * (x)), (40 * (y)))
                    pnl_grilleJeu.Controls.Add(newpic)
                    newpic.AllowDrop = False
                    newpic.BackColor = Color.Gray
                End If
                AddHandler newpic.MouseMove, AddressOf picJeu_MouseMove
                AddHandler newpic.DragEnter, AddressOf picGrille_DragEnter
                AddHandler newpic.DragDrop, AddressOf picTableau_DragDrop
                AddHandler newpic.DragDrop, AddressOf AgrandirTab
            Next
        Next
        PicDeckJoueur(0) = pic_pions1
        PicDeckJoueur(1) = pic_pions2
        PicDeckJoueur(2) = pic_pions3
        PicDeckJoueur(3) = pic_pions4
        PicDeckJoueur(4) = pic_pions5
        PicDeckJoueur(5) = pic_pions6
        lbl_deckJ.Text = " Deck : " + SaisieAgePrenom.Joueurs(0).getName
        For nbrePion As Integer = 0 To 5
            PicDeckJoueur(nbrePion).Image = My.Resources.ResourceManager.GetObject(SaisieAgePrenom.Joueurs(0).getPionI(nbrePion))
        Next
        lbl_classJ1.Enabled = False
        lbl_J1Point.Enabled = False
        lbl_classJ2.Enabled = False
        lbl_J1Point.Enabled = False
        lbl_classJ3.Enabled = False
        lbl_J1Point.Enabled = False
        lbl_classJ4.Enabled = False
        lbl_J1Point.Enabled = False
        pic_corbeille.AllowDrop = True
        ListeOrdreJoueurs = Jeu.OrdreJoueurs(OrdreDesJoueurs.ListePionsPosables, SaisieAgePrenom.Joueurs)

        For compteurJoueur = 0 To (Integer.Parse(SelectionNombreJoueur.cbx_Nbjoueur.Text) - 1)
            Select Case compteurJoueur
                Case 0
                    lbl_classJ1.Enabled = True
                    lbl_classJ1.Text = "Joueur 1 : " + SaisieAgePrenom.Joueurs(ListeOrdreJoueurs(compteurJoueur)).getName
                    lbl_J1Point.Enabled = True
                    lbl_J1Point.Text = 0
                Case 1
                    lbl_classJ2.Enabled = True
                    lbl_classJ2.Text = "Joueur 2 : " + SaisieAgePrenom.Joueurs(ListeOrdreJoueurs(compteurJoueur)).getName
                    lbl_J2Point.Enabled = True
                    lbl_J2Point.Text = 0
                Case 2
                    lbl_classJ3.Enabled = True
                    lbl_classJ3.Text = "Joueur 3 : " + SaisieAgePrenom.Joueurs(ListeOrdreJoueurs(compteurJoueur)).getName
                    lbl_J3Point.Enabled = True
                    lbl_J3Point.Text = 0
                Case 3
                    lbl_classJ4.Enabled = True
                    lbl_classJ4.Text = "Joueur 4 : " + SaisieAgePrenom.Joueurs(ListeOrdreJoueurs(compteurJoueur)).getName
                    lbl_J4Point.Enabled = True
                    lbl_J4Point.Text = 0
            End Select

        Next
    End Sub

    Private Sub pic_pions1_DragEnter(sender As Object, e As DragEventArgs) Handles pic_pions6.DragEnter, pic_pions5.DragEnter, pic_pions4.DragEnter, pic_pions3.DragEnter, pic_pions2.DragEnter, pic_pions1.DragEnter
        Dim pic As PictureBox = sender
        If pic.Image IsNot Nothing Then
            pic.AllowDrop = False
        Else
            If e.Data.GetDataPresent(DataFormats.Bitmap) Then
                e.Effect = DragDropEffects.Move
            Else
                e.Effect = DragDropEffects.None
            End If
        End If
    End Sub

    Private Sub pic_pions1_DragDrop(sender As Object, e As DragEventArgs) Handles pic_pions6.DragDrop, pic_pions5.DragDrop, pic_pions4.DragDrop, pic_pions3.DragDrop, pic_pions2.DragDrop, pic_pions1.DragDrop
        Dim pic As PictureBox = sender
        pic.Image = e.Data.GetData(DataFormats.Bitmap)
    End Sub

    Private Sub Btn_valider_Click(sender As Object, e As EventArgs) Handles btn_valider.Click
        PictureBoxTemporary.Clear()
        parcours = 0
        Dim pic As PictureBox

        If firstpart = True Then
            compteurJoueur = 1
            firstpart = False
        End If
        If compteurJoueur = Integer.Parse(SelectionNombreJoueur.cbx_Nbjoueur.Text) Then
            compteurJoueur = 0
        End If
        For nbpion = 0 To 5
            If PicDeckJoueur(nbpion).Image Is Nothing Then
                'MessageBox.Show("Ajout pion")
                Dim Random As Integer
                Dim Generator As System.Random = New System.Random()
                Random = Generator.Next(0, SaisieAgePrenom.deck.getNombreDeck() - 1)
                Integer.Parse(Random)
                SaisieAgePrenom.Joueurs(compteurJoueur).RetirerPionDeck(nbpion)
                PicDeckJoueur(nbpion).Image = Nothing
                SaisieAgePrenom.Joueurs(compteurJoueur).DeckDuJoueur.Add(SaisieAgePrenom.deck.DeckJeu(Random))
                SaisieAgePrenom.deck.Retirer(Random)
            End If
        Next
        lbl_deckJ.Text = " Deck : " + SaisieAgePrenom.Joueurs(compteurJoueur).getName
        For nbrePion As Integer = 0 To 5
            PicDeckJoueur(nbrePion).Image = My.Resources.ResourceManager.GetObject(SaisieAgePrenom.Joueurs(compteurJoueur).getPionI(nbrePion))
        Next
        compteurJoueur += 1
        Dim index As Integer = var
        For y = 0 To gridgame
            For x = 0 To gridgame
                If Picturebox(index).Image IsNot Nothing Then
                    RemoveHandler Picturebox(index).MouseMove, AddressOf picJeu_MouseMove
                End If
                index += 1
            Next
        Next
        lbl_compteurpioche.Text = SaisieAgePrenom.deck.getNombreDeck()

        If (numjoueur = 0) Then
            If (lbl_J1Point.Enabled = True) Then
                lbl_J1Point.Text = Jeu.compteversladroite(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text) + Jeu.compteverslagauche(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text) + Jeu.compteverslebas(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text) + Jeu.compteverslehaut(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text)
            End If
            numjoueur += 1

        ElseIf (numjoueur = 1) Then
            If (lbl_J2Point.Enabled = True) Then
                lbl_J2Point.Text = Jeu.compteversladroite(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text) + Jeu.compteverslagauche(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text) + Jeu.compteverslebas(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text) + Jeu.compteverslehaut(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text)
            End If
            If (SelectionNombreJoueur.cbx_Nbjoueur.Text = 2) Then
                numjoueur = 0
            Else
                numjoueur += 1
            End If

        ElseIf (numjoueur = 2) Then
            If (lbl_J3Point.Enabled = True) Then
                lbl_J3Point.Text = Jeu.compteversladroite(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text) + Jeu.compteverslagauche(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text) + Jeu.compteverslebas(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text) + Jeu.compteverslehaut(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text)
            End If
            If (SelectionNombreJoueur.cbx_Nbjoueur.Text = 3) Then
                numjoueur = 0
            Else
                numjoueur += 1
            End If

        ElseIf (numjoueur = 3) Then
            If (lbl_J4Point.Enabled = True) Then
                lbl_J4Point.Text = Jeu.compteversladroite(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text) + Jeu.compteverslagauche(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text) + Jeu.compteverslebas(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text) + Jeu.compteverslehaut(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text)
            End If
            numjoueur = 0
        End If
    End Sub

    Private Sub AgrandirTab(sender As Object, e As DragEventArgs)   'DragDrop des pictures box de la grille de jeu
        Dim pic As PictureBox = sender
        Dim haut, bas, gauche, droite As Integer
        Dim i As Integer = 0
        pic.Image = e.Data.GetData(DataFormats.Bitmap)
        Dim lex = Integer.Parse(pic.Name(3))
        Dim ley = Integer.Parse(pic.Name(4))
        For x = lex - 1 To lex + 1
            For y = ley - 1 To ley + 1
                If ley = y Then
                    Picturebox(x * 11 + y).BackColor = Color.Green
                    Picturebox(x * 11 + y).AllowDrop = True
                ElseIf lex = x Then
                    Picturebox(x * 11 + y).BackColor = Color.Green
                    Picturebox(x * 11 + y).AllowDrop = True
                End If
            Next
        Next

        For Each picb In picboxjoue
            memoire = lex * 11 + ley
            If Picturebox((lex - 1) * 11 + ley).Image IsNot Nothing AndAlso ((lex - 1) * 11 + ley <> memoire) Then
                While Picturebox(gauche * 11 + ley).Image IsNot Nothing
                    gauche = lex
                    Jeu.compteverslagauche(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text)
                    gauche = lex - 1
                End While
                memoire = (lex - 1) * 11 + ley
            End If
            If Picturebox((lex + 1) * 11 + ley).Image IsNot Nothing AndAlso ((lex + 1) * 11 + ley <> memoire) Then
                While Picturebox(droite * 11 + ley).Image IsNot Nothing
                    droite = lex
                    Jeu.compteversladroite(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text)
                    droite = lex + 1
                End While
                memoire = (lex + 1) * 11 + ley
            End If
            If Picturebox(lex * 11 + ley - 1).Image IsNot Nothing AndAlso (lex * 11 + ley - 1 <> memoire) Then
                While Picturebox(lex * 11 - haut).Image IsNot Nothing
                    haut = ley
                    Jeu.compteverslehaut(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text)
                    haut = ley - 1
                End While
                memoire = lex * 11 + ley - 1
            End If
            If Picturebox(lex * 11 + ley + 1).Image IsNot Nothing AndAlso (lex * 11 + ley + 1 <> memoire) Then
                While Picturebox(lex * 11 + bas).Image IsNot Nothing
                    bas = ley
                    Jeu.compteverslebas(ListeOrdreJoueurs(numjoueur), lbl_J1Point.Text, lbl_J2Point.Text, lbl_J3Point.Text, lbl_J4Point.Text)
                    bas = ley + 1
                End While
                memoire = lex * 11 + ley + 1
            End If
        Next
        picboxjoue.Clear()
        RemoveHandler Picturebox(x * 11 + y).DragDrop, AddressOf AgrandirTab
    End Sub

    Private Sub Btn_annuler_Click(sender As Object, e As EventArgs) Handles Btn_annuler.Click
        Dim verif As Boolean = False
        Dim indiceverif As Integer = 0
        For indice = 0 To 5
            If PicDeckJoueur(indice).Image IsNot Nothing Then
                indiceverif += 1
            End If
        Next
        If indiceverif = 6 Then
            verif = True
        End If
        For indice = 0 To 5
            If PicDeckJoueur(indice).Image Is Nothing Then
                PicDeckJoueur(indice).Image = PictureBoxTemporary(parcours).Image
                Exit For
            End If
        Next
        If verif = False Then
            PictureBoxTemporary(parcours).Image = Nothing
            parcours += 1
        End If
    End Sub

    Private Sub Pic_corbeille_DragDrop(sender As Object, e As DragEventArgs) Handles pic_corbeille.DragDrop
        Dim pic As PictureBox = sender
        pic.Image = e.Data.GetData(DataFormats.Bitmap)
        Dim pion As Integer
        For nbpion = 0 To 5
            If PicDeckJoueur(nbpion).Image Is Nothing Then
                pion = nbpion
            End If
        Next
        If compteurJoueur = Integer.Parse(SelectionNombreJoueur.cbx_Nbjoueur.Text) Then
            compteurJoueur = 0
        End If
        If firstpart = True Then
            SaisieAgePrenom.deck.Ajouter(SaisieAgePrenom.Joueurs(0).getPionI(pion))
            pic.Image = Nothing
        Else
            SaisieAgePrenom.deck.Ajouter(SaisieAgePrenom.Joueurs(compteurJoueur).getPionI(pion))
            pic.Image = Nothing
        End If
        lbl_compteurpioche.Text = SaisieAgePrenom.deck.getNombreDeck()
    End Sub

    Private Sub Pic_corbeille_DragEnter(sender As Object, e As DragEventArgs) Handles pic_corbeille.DragEnter
        Dim pic As PictureBox = sender
        If pic.Image IsNot Nothing Then
            pic.AllowDrop = False
        Else
            If e.Data.GetDataPresent(DataFormats.Bitmap) Then
                e.Effect = DragDropEffects.Move
            Else
                e.Effect = DragDropEffects.None
            End If
        End If
        pic.Image = Nothing
    End Sub

    Private Sub Pic_corbeille_MouseMove(sender As Object, e As MouseEventArgs) Handles pic_corbeille.MouseMove
        Dim pic As PictureBox = sender
        Dim rep As DragDropEffects
        If e.Button = MouseButtons.Left AndAlso pic.Image IsNot Nothing Then
            pic.AllowDrop = False
            rep = pic.DoDragDrop(pic.Image, DragDropEffects.Move)
            If rep = DragDropEffects.Move Then
                pic.Image = Nothing
            End If
            pic.AllowDrop = True
        End If
    End Sub

    Private Sub Cmd_menu_Click(sender As Object, e As EventArgs) Handles cmd_menu.Click
        SaisieAgePrenom.Close()
        OrdreDesJoueurs.Close()
        Me.Close()
        MenuQwirkle.Show()
    End Sub
End Class
