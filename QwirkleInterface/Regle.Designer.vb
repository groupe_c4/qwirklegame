﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Regle
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Regle))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Lbl_ButJeu = New System.Windows.Forms.Label()
        Me.cmd_retour = New System.Windows.Forms.Button()
        Me.Lbl_DeroulementPartie = New System.Windows.Forms.Label()
        Me.Lbl_ButjeuText = New System.Windows.Forms.Label()
        Me.Lbl_DeroulementPartieText = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Yu Gothic UI", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(180, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(516, 81)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Règles du Quirkle"
        '
        'Lbl_ButJeu
        '
        Me.Lbl_ButJeu.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Lbl_ButJeu.AutoSize = True
        Me.Lbl_ButJeu.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_ButJeu.Font = New System.Drawing.Font("Yu Gothic UI", 19.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_ButJeu.ForeColor = System.Drawing.Color.Gray
        Me.Lbl_ButJeu.Location = New System.Drawing.Point(32, 121)
        Me.Lbl_ButJeu.Name = "Lbl_ButJeu"
        Me.Lbl_ButJeu.Size = New System.Drawing.Size(195, 46)
        Me.Lbl_ButJeu.TabIndex = 6
        Me.Lbl_ButJeu.Text = "But du jeu :"
        '
        'cmd_retour
        '
        Me.cmd_retour.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmd_retour.BackColor = System.Drawing.Color.Red
        Me.cmd_retour.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmd_retour.Font = New System.Drawing.Font("Yu Gothic UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_retour.Location = New System.Drawing.Point(687, 594)
        Me.cmd_retour.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmd_retour.Name = "cmd_retour"
        Me.cmd_retour.Size = New System.Drawing.Size(176, 52)
        Me.cmd_retour.TabIndex = 8
        Me.cmd_retour.Text = "RETOUR"
        Me.cmd_retour.UseVisualStyleBackColor = False
        '
        'Lbl_DeroulementPartie
        '
        Me.Lbl_DeroulementPartie.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Lbl_DeroulementPartie.AutoSize = True
        Me.Lbl_DeroulementPartie.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_DeroulementPartie.Font = New System.Drawing.Font("Yu Gothic UI", 19.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_DeroulementPartie.ForeColor = System.Drawing.Color.Gray
        Me.Lbl_DeroulementPartie.Location = New System.Drawing.Point(32, 299)
        Me.Lbl_DeroulementPartie.Name = "Lbl_DeroulementPartie"
        Me.Lbl_DeroulementPartie.Size = New System.Drawing.Size(404, 46)
        Me.Lbl_DeroulementPartie.TabIndex = 9
        Me.Lbl_DeroulementPartie.Text = "Déroulement de la partie"
        '
        'Lbl_ButjeuText
        '
        Me.Lbl_ButjeuText.AutoSize = True
        Me.Lbl_ButjeuText.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_ButjeuText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_ButjeuText.ForeColor = System.Drawing.Color.White
        Me.Lbl_ButjeuText.Location = New System.Drawing.Point(38, 182)
        Me.Lbl_ButjeuText.Name = "Lbl_ButjeuText"
        Me.Lbl_ButjeuText.Size = New System.Drawing.Size(533, 108)
        Me.Lbl_ButjeuText.TabIndex = 10
        Me.Lbl_ButjeuText.Text = resources.GetString("Lbl_ButjeuText.Text")
        '
        'Lbl_DeroulementPartieText
        '
        Me.Lbl_DeroulementPartieText.AutoSize = True
        Me.Lbl_DeroulementPartieText.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_DeroulementPartieText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_DeroulementPartieText.ForeColor = System.Drawing.Color.White
        Me.Lbl_DeroulementPartieText.Location = New System.Drawing.Point(38, 359)
        Me.Lbl_DeroulementPartieText.Name = "Lbl_DeroulementPartieText"
        Me.Lbl_DeroulementPartieText.Size = New System.Drawing.Size(549, 270)
        Me.Lbl_DeroulementPartieText.TabIndex = 11
        Me.Lbl_DeroulementPartieText.Text = resources.GetString("Lbl_DeroulementPartieText.Text")
        '
        'Regle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.QwirkleInterface.My.Resources.Resources.wallpaper
        Me.ClientSize = New System.Drawing.Size(877, 660)
        Me.Controls.Add(Me.Lbl_DeroulementPartieText)
        Me.Controls.Add(Me.Lbl_ButjeuText)
        Me.Controls.Add(Me.Lbl_DeroulementPartie)
        Me.Controls.Add(Me.cmd_retour)
        Me.Controls.Add(Me.Lbl_ButJeu)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "Regle"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Règles"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Lbl_ButJeu As Label
    Friend WithEvents cmd_retour As Button
    Friend WithEvents Lbl_DeroulementPartie As Label
    Friend WithEvents Lbl_ButjeuText As Label
    Friend WithEvents Lbl_DeroulementPartieText As Label
End Class
