﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SaisieAgePrenom
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SaisieAgePrenom))
        Me.lbl_compteur = New System.Windows.Forms.Label()
        Me.cmd_retour = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txt_prenom = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txt_age = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmd_valider = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_compteur
        '
        Me.lbl_compteur.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_compteur.AutoSize = True
        Me.lbl_compteur.BackColor = System.Drawing.Color.Transparent
        Me.lbl_compteur.Font = New System.Drawing.Font("Yu Gothic UI", 33.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_compteur.ForeColor = System.Drawing.Color.White
        Me.lbl_compteur.Location = New System.Drawing.Point(212, 69)
        Me.lbl_compteur.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_compteur.Name = "lbl_compteur"
        Me.lbl_compteur.Size = New System.Drawing.Size(233, 60)
        Me.lbl_compteur.TabIndex = 3
        Me.lbl_compteur.Text = "Joueurs n°"
        '
        'cmd_retour
        '
        Me.cmd_retour.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmd_retour.BackColor = System.Drawing.Color.Red
        Me.cmd_retour.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmd_retour.Font = New System.Drawing.Font("Yu Gothic UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_retour.Location = New System.Drawing.Point(515, 483)
        Me.cmd_retour.Margin = New System.Windows.Forms.Padding(2)
        Me.cmd_retour.Name = "cmd_retour"
        Me.cmd_retour.Size = New System.Drawing.Size(132, 42)
        Me.cmd_retour.TabIndex = 4
        Me.cmd_retour.Text = "RETOUR"
        Me.cmd_retour.UseVisualStyleBackColor = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.txt_prenom)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel2)
        Me.FlowLayoutPanel1.Controls.Add(Me.txt_age)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmd_valider)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(212, 204)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(2)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(234, 251)
        Me.FlowLayoutPanel1.TabIndex = 5
        '
        'txt_prenom
        '
        Me.txt_prenom.Font = New System.Drawing.Font("Yu Gothic UI", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_prenom.ForeColor = System.Drawing.Color.Silver
        Me.txt_prenom.Location = New System.Drawing.Point(2, 2)
        Me.txt_prenom.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_prenom.Name = "txt_prenom"
        Me.txt_prenom.Size = New System.Drawing.Size(232, 43)
        Me.txt_prenom.TabIndex = 7
        Me.txt_prenom.Text = "Prénom"
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(2, 49)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(230, 42)
        Me.Panel2.TabIndex = 6
        '
        'txt_age
        '
        Me.txt_age.Font = New System.Drawing.Font("Yu Gothic UI", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_age.ForeColor = System.Drawing.Color.Silver
        Me.txt_age.Location = New System.Drawing.Point(2, 95)
        Me.txt_age.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_age.Name = "txt_age"
        Me.txt_age.Size = New System.Drawing.Size(232, 43)
        Me.txt_age.TabIndex = 8
        Me.txt_age.Text = "Age"
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(2, 142)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(230, 42)
        Me.Panel1.TabIndex = 9
        '
        'cmd_valider
        '
        Me.cmd_valider.BackColor = System.Drawing.Color.DodgerBlue
        Me.cmd_valider.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmd_valider.Font = New System.Drawing.Font("Yu Gothic UI", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_valider.Location = New System.Drawing.Point(2, 188)
        Me.cmd_valider.Margin = New System.Windows.Forms.Padding(2)
        Me.cmd_valider.Name = "cmd_valider"
        Me.cmd_valider.Size = New System.Drawing.Size(230, 58)
        Me.cmd_valider.TabIndex = 10
        Me.cmd_valider.Text = "VALIDER"
        Me.cmd_valider.UseVisualStyleBackColor = False
        '
        'SaisieAgePrenom
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.QwirkleInterface.My.Resources.Resources.wallpaper
        Me.ClientSize = New System.Drawing.Size(658, 536)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.cmd_retour)
        Me.Controls.Add(Me.lbl_compteur)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "SaisieAgePrenom"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sasie Age Prenom"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_compteur As Label
    Friend WithEvents cmd_retour As Button
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents txt_prenom As TextBox
    Friend WithEvents txt_age As TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents cmd_valider As Button
End Class
