﻿Imports QwirkleLibrary

Public Class SaisieAgePrenom

    Dim nombrejoueur As Integer
    Public Joueurs As List(Of Joueur) = New List(Of Joueur)
    Dim compteurDeJoueur As Integer
    Public deck = New Deck()

    Private Sub Cmd_retour_Click(sender As Object, e As EventArgs) Handles cmd_retour.Click
        SelectionNombreJoueur.Show()
        Me.Close()
    End Sub

    Private Sub SaisieAgePrenom_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.nombrejoueur = Integer.Parse(SelectionNombreJoueur.cbx_Nbjoueur.Text)
        compteurDeJoueur = 1
        lbl_compteur.Text = "Joueur n° " + compteurDeJoueur.ToString()
    End Sub

    Private Sub Txt_prenom_Click(sender As Object, e As EventArgs) Handles txt_prenom.Click
        txt_prenom.Text = Nothing
        txt_prenom.ForeColor = Color.Black
    End Sub

    Private Sub Txt_age_Click(sender As Object, e As EventArgs) Handles txt_age.Click
        txt_age.Text = Nothing
        txt_age.ForeColor = Color.Black
    End Sub

    Private Sub Cmd_valider_Click(sender As Object, e As EventArgs) Handles cmd_valider.Click
        Dim erreur As Boolean
        Dim prenom = txt_prenom.Text
        Dim age As Integer
        Integer.TryParse(txt_age.Text, age)
        erreur = False
        If prenom.Length < 1 Then
            MessageBox.Show("Veuillez saisir un prenom correct")
            erreur = True
        End If
        If age = 0 Or age > 130 Then
            MessageBox.Show("Veuillez rentrer un nombre correct")
            erreur = True
        End If
        If age > 0 And prenom.Length >= 1 Then
            If (compteurDeJoueur <> nombrejoueur + 1) Then
                Joueurs.Add(New Joueur(prenom, age, deck))
            End If
        End If
        If erreur <> True Then
            compteurDeJoueur += 1
            lbl_compteur.Text = "Joueur n° " + compteurDeJoueur.ToString()
            txt_prenom.ForeColor = Color.Silver
            txt_age.ForeColor = Color.Silver
            txt_prenom.Text = "Prénom"
            txt_age.Text = "Age"
        End If
        If (compteurDeJoueur = nombrejoueur + 1) Then
            OrdreDesJoueurs.Show()
            Me.Hide()
        End If
    End Sub
End Class