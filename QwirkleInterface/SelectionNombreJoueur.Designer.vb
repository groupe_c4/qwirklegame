﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SelectionNombreJoueur
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SelectionNombreJoueur))
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cbx_Nbjoueur = New System.Windows.Forms.ComboBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmd_valider = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmd_retour = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.cbx_Nbjoueur)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel2)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmd_valider)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(424, 346)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(468, 296)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'cbx_Nbjoueur
        '
        Me.cbx_Nbjoueur.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cbx_Nbjoueur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Nbjoueur.Font = New System.Drawing.Font("Yu Gothic UI", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbx_Nbjoueur.ForeColor = System.Drawing.Color.Black
        Me.cbx_Nbjoueur.FormattingEnabled = True
        Me.cbx_Nbjoueur.Items.AddRange(New Object() {"2", "3", "4"})
        Me.cbx_Nbjoueur.Location = New System.Drawing.Point(4, 4)
        Me.cbx_Nbjoueur.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbx_Nbjoueur.Name = "cbx_Nbjoueur"
        Me.cbx_Nbjoueur.Size = New System.Drawing.Size(460, 79)
        Me.cbx_Nbjoueur.Sorted = True
        Me.cbx_Nbjoueur.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(4, 91)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(460, 81)
        Me.Panel2.TabIndex = 6
        '
        'cmd_valider
        '
        Me.cmd_valider.BackColor = System.Drawing.Color.DodgerBlue
        Me.cmd_valider.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmd_valider.Font = New System.Drawing.Font("Yu Gothic UI", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_valider.Location = New System.Drawing.Point(4, 180)
        Me.cmd_valider.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmd_valider.Name = "cmd_valider"
        Me.cmd_valider.Size = New System.Drawing.Size(460, 112)
        Me.cmd_valider.TabIndex = 5
        Me.cmd_valider.Text = "VALIDER"
        Me.cmd_valider.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Yu Gothic UI", 33.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(52, 133)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(1209, 116)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Entrez un nombre de joueurs"
        '
        'cmd_retour
        '
        Me.cmd_retour.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmd_retour.BackColor = System.Drawing.Color.Red
        Me.cmd_retour.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmd_retour.Font = New System.Drawing.Font("Yu Gothic UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_retour.Location = New System.Drawing.Point(1034, 931)
        Me.cmd_retour.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmd_retour.Name = "cmd_retour"
        Me.cmd_retour.Size = New System.Drawing.Size(264, 81)
        Me.cmd_retour.TabIndex = 3
        Me.cmd_retour.Text = "RETOUR"
        Me.cmd_retour.UseVisualStyleBackColor = False
        '
        'SelectionNombreJoueur
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.QwirkleInterface.My.Resources.Resources.wallpaper
        Me.ClientSize = New System.Drawing.Size(1316, 1031)
        Me.Controls.Add(Me.cmd_retour)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "SelectionNombreJoueur"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selection du Nombre Joueur"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents cbx_Nbjoueur As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cmd_valider As Button
    Friend WithEvents cmd_retour As Button
    Friend WithEvents Panel2 As Panel
End Class
