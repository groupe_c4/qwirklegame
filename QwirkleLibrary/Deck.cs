﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleLibrary
{
    public class Deck
    {
        private string nomTemp;
        public string[] couleurs = { "b", "y", "r", "o", "p", "g" };
        public static string[] formes = { "1", "2", "3", "4", "5", "6" };
        // 1 : losange pas entier, 2:trefle, 3:etoile, 4:losange, 5: carré, 6:cercle

        public List<String> DeckJeu = new List<String>();
        public Deck()
        {
            for(int triple = 0; triple < 3; triple++)
            {
                for(int couleurS = 0; couleurS < 6; couleurS++)
                {
                    for(int formeS = 0; formeS < 6; formeS++)
                    {
                        nomTemp = string.Concat(couleurs[couleurS], formes[formeS]);
						DeckJeu.Add(nomTemp);
                    }                    
                }
            }
        }

        public int getNombreDeck()
        {
            return this.DeckJeu.Count;
        }

        public void Retirer(int indexRetirer)
		{
			this.DeckJeu.RemoveAt(indexRetirer);		
		}

        public void Ajouter(string namepion)
        {
            this.DeckJeu.Add(namepion);
        }
    }
}
