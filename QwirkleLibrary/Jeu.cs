﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleLibrary
{
    public class Jeu
    {
        private static int firstPlayer;
        private int playerNumber;
        private List<int> scoreBoard = new List<int>();

        public static List<int> OrdreJoueurs(List<int> PawsPlayable, List<Joueur> Joueurs)
        {
            int max = 0;
            int indexFP = 0;
            int compteurJ = 0;
            int nbjoueur = Joueurs.Count;
            List<int> OrdreJoueurs = new List<int>();
            foreach (int PionsJActuel in PawsPlayable)
            {
                if (max < PionsJActuel)
                {
                    max = PionsJActuel;
                    indexFP = compteurJ;
                }
                else if ((max == PionsJActuel) && (Joueurs[PionsJActuel].getAge() > Joueurs[indexFP].getAge()))
                {
                    max = PionsJActuel;
                    indexFP = compteurJ;
                }
                else
                {
                    max = max;
                    indexFP = indexFP;
                }
                compteurJ++;
            }
            firstPlayer = indexFP;
            int test = firstPlayer;
            while (OrdreJoueurs.Count != Joueurs.Count)
            {
                if (test >= Joueurs.Count)
                {
                    test = 0;
                }
                OrdreJoueurs.Add(test);
                test++;
            }
            return OrdreJoueurs;
        }

        /*
        public int getPlayerNumber()
        {
            return this.playerNumber;
        }

        public int checkPawnsPlayable()
        {

        }
        */

         
        public int points;

        public static int compteverslagauche(int joueur, int pointsj1, int pointsj2, int pointsj3, int pointsj4)
        {
            int longueur; //longueur de la ligne de tuile


            if (joueur == 1)
            {
                longueur = 0;
                pointsj1 = pointsj1 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj1 = pointsj1 + 6;
                }
                longueur = 0;
                return pointsj1;
            }

            else if(joueur == 2)
            {
                longueur = 0;
                pointsj2 = pointsj2 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj2 = pointsj2 + 6;
                }
                longueur = 0;
                return pointsj2;
            }
            else if(joueur == 3)
            {
                longueur = 0;
                pointsj3 = pointsj3 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj3 = pointsj3 + 6;
                }
                longueur = 0;
                return pointsj3;
            }
            else if(joueur ==4)
            {
                longueur = 0;
                pointsj4 = pointsj4 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj4 = pointsj4 + 6;
                }
                longueur = 0;
                return pointsj4;
            }

            return 0;
            
        }

        public static int compteversladroite(int joueur, int pointsj1, int pointsj2, int pointsj3, int pointsj4)
        {
            int longueur; //longueur de la ligne de tuile



            if (joueur == 1)
            {
                longueur = 0;
                pointsj1 = pointsj1 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj1 = pointsj1 + 6;
                }
                longueur = 0;
                return pointsj1;
            }
            else if (joueur == 2)
            {
                longueur = 0;
                pointsj2 = pointsj2 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj2 = pointsj2 + 6;
                }
                longueur = 0;
                return pointsj2;
            }
            else if (joueur == 3)
            {
                longueur = 0;
                pointsj3 = pointsj3 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj3 = pointsj3 + 6;
                }
                longueur = 0;
                return pointsj3;
            }
            else if (joueur == 4)
            {
                longueur = 0;
                pointsj4 = pointsj4 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj4 = pointsj4 + 6;
                }
                longueur = 0;
                return pointsj4;
            }

            return 0;
        }

        public static int compteverslehaut(int joueur, int pointsj1, int pointsj2, int pointsj3, int pointsj4)
        {
            int longueur; //longueur de la ligne de tuile



            if (joueur == 1)
            {
                longueur = 0;
                pointsj1 = pointsj1 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj1 = pointsj1 + 6;
                }
                longueur = 0;
                return pointsj1;
            }
            else if (joueur == 2)
            {
                longueur = 0;
                pointsj2 = pointsj2 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj2 = pointsj2 + 6;
                }
                longueur = 0;
                return pointsj2;
            }
            else if (joueur == 3)
            {
                longueur = 0;
                pointsj3 = pointsj3 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj3 = pointsj3 + 6;
                }
                longueur = 0;
                return pointsj3;
            }
            else if (joueur == 4)
            {
                longueur = 0;
                pointsj4 = pointsj4 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj4 = pointsj4 + 6;
                }
                longueur = 0;
                return pointsj4;
            }

            return 0;
        }

        public static int compteverslebas(int joueur, int pointsj1, int pointsj2, int pointsj3, int pointsj4)
        {
            int longueur; //longueur de la ligne de tuile



            if (joueur == 1)
            {
                longueur = 0;
                pointsj1 = pointsj1 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj1 = pointsj1 + 6;
                }
                longueur = 0;
                return pointsj1;
            }
            else if (joueur == 2)
            {
                longueur = 0;
                pointsj2 = pointsj2 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj2 = pointsj2 + 6;
                }
                longueur = 0;
                return pointsj2;
            }
            else if (joueur == 3)
            {
                longueur = 0;
                pointsj3 = pointsj3 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj3 = pointsj3 + 6;
                }
                longueur = 0;
                return pointsj3;
            }
            else if (joueur == 4)
            {
                longueur = 0;
                pointsj4 = pointsj4 + 1;

                longueur = longueur + 1;

                if (longueur >= 6)
                {
                    pointsj4 = pointsj4 + 6;
                }
                longueur = 0;
                return pointsj4;
            }

            return 0;

        }
        /*
        
        public void skipTurn()
        {

        }
        
        public void endGame()
        {

        }
        */
    }
}
