﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleLibrary
{
    public class Joueur
    {
        private string Name;
        private int Age;
        private int playerScore;
        public List<String> DeckDuJoueur = new List<String>();


        public Joueur(string Name, int Age, Deck deck)
        {
            this.Name = Name;
            this.Age = Age;
            this.playerScore = 0;
            Random aleatoire = new Random();
            for (int sixcartes = 0; sixcartes < 6; sixcartes++)
            {
                int aleatoireD = aleatoire.Next(0, deck.getNombreDeck() - 1);
                DeckDuJoueur.Add(deck.DeckJeu[aleatoireD]);
                deck.DeckJeu.RemoveAt(aleatoireD);
            }
        }
        public void RetirerPionDeck(int indexretirer)
        {
            this.DeckDuJoueur.RemoveAt(indexretirer);
        }

        public string getName()
        {
            return this.Name;
        }

        public int getAge()
        {
            return this.Age;
        }

        public int getScore()
        {
            return this.playerScore;
        }

        public string getPionI(int nombre)
        {
             return this.DeckDuJoueur[nombre];
        }
    }
}
