﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleLibrary;

namespace QwirkleTest
{
    [TestClass]
    public class DeckTest
    {
        [TestMethod]
        public void NombreTuilesDeckInit()
        {
            Deck deck = new Deck();
            Assert.AreEqual(108, deck.getNombreDeck());
        }

        [TestMethod]
        public void TestRemove()
        {
            Deck deck = new Deck();
            deck.Retirer(102);
            Assert.AreEqual(107, deck.getNombreDeck());
            deck.Retirer(12);
            Assert.AreEqual(106, deck.getNombreDeck());

        }


    }
}
