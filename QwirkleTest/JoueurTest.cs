﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleLibrary;

namespace QwirkleTest
{
    [TestClass]
    public class JoueurTest
    {
        [TestMethod]
        public void JoueurNom()
        {
            Deck deck = new Deck();
            Joueur j1 = new Joueur("Thierry", 19, deck);
            Joueur j2 = new Joueur("Jean", 12, deck);
            Assert.AreEqual("Thierry", j1.getName());
            Assert.AreEqual("Jean", j2.getName());
        }

        [TestMethod]
        public void JoueurAge()
        {
            Deck deck = new Deck();
            Joueur j1 = new Joueur("Thierry", 19, deck);
            Joueur j2 = new Joueur("Jean", 12, deck);
            Assert.AreEqual(19, j1.getAge());
            Assert.AreEqual(12, j2.getAge());
        }

    }
}
